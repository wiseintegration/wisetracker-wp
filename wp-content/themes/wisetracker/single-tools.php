<?php get_header(); ?>
  <div class="page-head"></div>
  <main class="main--global main--sidebars">
    <?php get_template_part('sidebar-left'); ?>
    <section class="content content--main">

      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <h1>
          <?php the_title(); ?>
          <small class="color--faded">(<?php echo get_field('item') ? count(get_field('item')) : '0'; ?>)</small>
        </h1>
        <hr>
        <div class="container--fluid">
          <div class="grid">
            <?php
            if( have_rows('item') ):
              $i = 0;
              while ( have_rows('item') ) : the_row();
                $images = get_sub_field('image');
                $i++;
            ?>
            <div class="col col--md--4 col--sm--6 mb--sm--0 mb--2 d--flex">
              <div class="toolCard">
                <div class="toolCard-image <?php echo count($images) > 1 ? 'toolCard-image--gallery' : 'toolCard-image-only' ?>">
                  <?php if( $images ): ?>
                    <?php foreach( $images as $key => $image ): ?>
                      <a href="<?php echo $image['url']; ?>" data-lightbox="gallery--<?= $i ?>" data-title="<?php echo $image['description']; ?>" data-gallery="Galerija">
                        <?php if($key == 0): ?>
                          <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['description']; ?>" />
                        <?php endif; ?>
                      </a>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <img src="<?php echo get_template_directory_uri() ?>/dist/images/placeholder-250x150.png" alt="" class="image--contain">
                  <?php endif; ?>
                </div>


                <div class="toolCard-block mt--2">
                  <h3 class="toolCard-title mb--0">
                    <?php the_sub_field('title'); ?>
                  </h3>
                  <p class="color--faded mb--2">
                    <?php the_sub_field('price'); ?>
                  </p>
                  <p class="toolCard-text">
                    <?php the_sub_field('description'); ?>
                  </p>
                </div>
              </div>
            </div>


            <?php
              endwhile;
              else :
            ?>
            <div class="col">
              <p class="toolCard-text color--faded text--leader">
                <?php _e( 'Sorry, nothing to display.', 'wisetracker' ); ?>
              </p>
            </div>
            <?php
              endif;
            ?>
          </div>
        </div>
      <?php endwhile; ?>

      <?php else: ?>

        <!-- article -->
        <div>
          <h2><?php _e( 'Sorry, nothing to display.', 'wisetracker' ); ?></h2>
        </div>
        <!-- /article -->

      <?php endif; ?>
    </section>
    <?php get_template_part('sidebar-right'); ?>
  </main>





<?php get_footer(); ?>
