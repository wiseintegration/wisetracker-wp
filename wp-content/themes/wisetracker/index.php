<?php get_header(); ?>

<div class="page-head"></div>
<main class="main--global main--sidebars">

	<?php get_template_part('sidebar-left'); ?>
	<section class="content content--main content--categoryList">

		<?php get_template_part('loop'); ?>
		<?php get_template_part('pagination'); ?>

	</section>
  <?php get_template_part('sidebar-right'); ?>

</main>
<?php get_footer(); ?>
