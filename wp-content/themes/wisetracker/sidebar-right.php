<aside class="sidebar sidebar--right">
  <h3 class=""><?php _e( 'Other', 'wisetracker' ); ?></h3>
  <div class="categories">
    <?php
    $args = array(
      'numberposts' => -1,
      'post_type'   => 'others',
    );
    $others = get_posts( $args );
    if($others):
      foreach ($others as $other):
        ?>
        <a class="categories-item" href="<?= $other->guid ?>"><?= $other->post_title ?></a>
        <?php
      endforeach;
    else:
      ?>
      <p class="color--faded"><?php _e( 'No categories', 'wisetracker' ); ?></p>
    <?php endif; ?>
  </div>
</aside>
