const gulp = require('gulp');
const config = require('../config');
const browserSync = require('browser-sync').create();

const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const globImporter = require('node-sass-glob-importer');


/**
 * Builds sass
 *
 * Command: gulp sass:prod
 *
 * @param {}
 *
 * @returns {}
 */



gulp.task('sass', () => {
  const postcssPlugins = [
    autoprefixer({
      browsers: ['> 0%']
    })
  ];
  const stream = gulp.src(['**/*.sass'], {cwd: config.paths.sass.src})
		.pipe(sassLint())
		.pipe(sassLint.format());

    // Don't fail in development enviroment
		if(process.env.NODE_ENV === 'production') {
			stream.pipe(sassLint.failOnError());
		}

		stream.pipe(sass({
    	importer: globImporter()
  	})
		.on('error', sass.logError))
    .pipe(postcss(postcssPlugins))
    .pipe(gulp.dest(config.paths.sass.dest));

  return stream;
});
