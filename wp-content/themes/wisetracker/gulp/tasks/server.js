const gulp = require('gulp');
const config = require('../config');
const browserSync = require('browser-sync').create();

/*
 *
 * Development
 *
 * Commands: gulp server
 *
*/

gulp.task('server', () => {
  browserSync.init(config.serverOptions);
});
