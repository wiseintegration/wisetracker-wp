const gulp = require('gulp');
const config = require('../config');
const webpack = require('webpack-stream');
const webpackConfig = require('../../webpack.config');
// const webpackDevConfig = require('../../webpack/webpack.dev');
// const webpackProdConfig = require('../../webpack/webpack.prod');

/**
 * Builds dev scripts
 *
 * Command: gulp js
 *
 * @param {}
 *
 * @returns {}
 */

gulp.task('js', function() {
  gulp.src(config.paths.js.all)
      .pipe(webpack(webpackConfig))
      .pipe(gulp.dest(config.paths.js.dest));
});
