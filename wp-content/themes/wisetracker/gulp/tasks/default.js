const gulp = require('gulp');
const config = require('../config');
const runSequence = require('run-sequence');
const browserSync = require('browser-sync').create();

/*
 *
 * Development
 *
 * Commands: gulp
 *
*/
gulp.task('default',['sass', 'js'], (done) => runSequence('server', () => {
	gulp.watch([config.paths.sass.all], ['sass']);
}));
