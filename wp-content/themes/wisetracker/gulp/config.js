const path = require('path');

const srcDir = path.resolve(__dirname, '../src');

const config = {
  url: 'http://wisetracker-wp.local',
  baseDir: './dist',
  paths: {
    project: './dist',
    sass: {
      src: './src/sass',
      all: './src/sass/**/*.sass',
      dest: './dist/css',
    },
    js: {
      src: './src/js',
      all: './src/js/**/*.js',
      dest: './dist/js',
      entry: {
        master: `${srcDir}/js/index.js`,
        'admin.bundle': `${srcDir}/js/admin.js`,
      },
    },
  },
  serverOptions: {
    files: [
      './dist/css/**/*.css',
      './dist/js/**/*.js',
      './**/*.php',
    ],
    injectChanges: true,
    notify: {
      styles: {
        top: 'auto',
        bottom: '0',
      },
    },
  },
};

/*
 * If url to another server is set in config.url
 * the following creates a proxy there.
 * Otherwise, it creates a local server.
 *
 */

if (!config.url) {
  // Create server
  config.serverOptions.server = {
    baseDir: config.baseDir,
  };
} else {
  // Use existing server setup
  config.serverOptions.proxy = config.url;
}


module.exports = config;
