import Vue from 'vue';
import store from './store/store';
import workersSearch from './components/workers-search.vue';

// Init Vue
new Vue({
  el: '#app',
  store,
  components: {
    'workers-search': workersSearch,
  },
  data: () => ({
  }),
});
