import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    totalWorkers: null,
    loader: false,
    pages: 1,
    page: 1,
    perPage: 8,
    workers: {},
  },
  getters: {
    totalWorkers(state) {
      return state.totalWorkers;
    },
    pages(state) {
      return state.pages;
    },
    page(state) {
      return state.page;
    },
    perPage(state) {
      return state.perPage;
    },
    workers(state) {
      return state.workers;
    },
    loader(state) {
      return state.loader;
    },
  },
  mutations: {
    fetchWorkers(state, days) {
      const apiUrl = '/wp-json/workers/v2/users';
      const requestParams = {
        available_days: days,
        page: state.page,
        per_page: state.perPage,
      };
      state.loader = true;
      axios.get(apiUrl, { params: requestParams }).then((response) => {
        state.workers = response.data.users;
        state.totalWorkers = response.data.countAll;
        state.pages = Math.ceil(state.totalWorkers / state.perPage);
      }).catch((error) => {
        // console.log(error);
      }).then(() => {
        state.loader = false;
      });
    },
  },
  actions: {
    fetchWorkers({ commit }, days) {
      commit('fetchWorkers', days);
    },
  },
});

export default store;
