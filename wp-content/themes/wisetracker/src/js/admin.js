import Vue from 'vue';

// Components
import datePicker from './components/date-picker.vue';

// Init
new Vue({
  el: '#workers-profile-edit',
  data: () => ({
    initDays: 'asdas',
  }),
  components: {
    'date-picker': datePicker,
  },
  methods: {},
});
