<?php

// function my_theme_load_theme_textdomain() {
// 	load_theme_textdomain( 'my-theme', get_template_directory() . '/languages' );
// }
// add_action( 'after_setup_theme', 'my_theme_load_theme_textdomain' );


// load_theme_textdomain( 'wisetracker', get_template_directory_uri().'/languages' );
function wisetracker_theme_setup() {
    load_theme_textdomain( 'wisetracker', get_template_directory() . '/languages' );

    $locale = get_locale();
    $locale_file = get_template_directory() . "/languages/$locale.php";

    if ( is_readable( $locale_file ) ) {
        require_once( $locale_file );
    }
}
add_action( 'after_setup_theme', 'wisetracker_theme_setup' );


// Create main nav
function register_global_nav() {
  register_nav_menus(
		array(
			'global-nav' => __( 'Global Navigation', 'wisetracker' ),
			'logged-in-menu' => __( 'Logged In menu', 'wisetracker' ),
			'logged-out-menu' => __( 'Logged Out menu', 'wisetracker' )
		)
	);
}
add_action( 'init', 'register_global_nav' );

// New thumbnail
add_image_size( 'worker-profile', 250, 250 );

add_action('init', 'post_type__others'); // Add our Others Post Type
add_action('init', 'post_type__tools'); // Add our Tools Post Type
/*------------------------------------*\
Custom Post Types
\*------------------------------------*/

// Create Tools post type
function post_type__others() {
	register_taxonomy_for_object_type('tools_categories', 'tools');
	register_post_type('tools',
	array(
		'labels' => array(
			'name' => __('Tools categories', 'wisetracker'),
			'singular_name' => __('Tools category', 'wisetracker'),
			'add_new' => __('Add New', 'wisetracker'),
			'add_new_item' => __('Add New Tools category', 'wisetracker'),
			'edit' => __('Edit', 'wisetracker'),
			'edit_item' => __('Edit Tools category', 'wisetracker'),
			'new_item' => __('New Tools category', 'wisetracker'),
			'view' => __('View Tools category', 'wisetracker'),
			'view_item' => __('View Tools category', 'wisetracker'),
			'search_items' => __('Search Tools categories', 'wisetracker'),
			'not_found' => __('No Tools categories found', 'wisetracker'),
			'not_found_in_trash' => __('No Tools categories found in Trash', 'wisetracker')
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => false,
		'show_in_nav_menus' => true,
		'supports' => array(
			'title',
			'editor',
			'thumbnail'
		),
		'can_export' => true
	));
}

// Create Others post type
function post_type__tools()
{
	register_taxonomy_for_object_type('others_category', 'others');
	register_post_type('others',
	array(
		'labels' => array(
			'name' => __('Others categories', 'wisetracker'), // Rename these to suit
			'singular_name' => __('Others category', 'wisetracker'),
			'add_new' => __('Add New', 'wisetracker'),
			'add_new_item' => __('Add New Others category', 'wisetracker'),
			'edit' => __('Edit', 'wisetracker'),
			'edit_item' => __('Edit Others category', 'wisetracker'),
			'new_item' => __('New Others category', 'wisetracker'),
			'view' => __('View Others category', 'wisetracker'),
			'view_item' => __('View Others category', 'wisetracker'),
			'search_items' => __('Search Others categories', 'wisetracker'),
			'not_found' => __('No Others categories found', 'wisetracker'),
			'not_found_in_trash' => __('No Others categories found in Trash', 'wisetracker')
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => false,
		'show_in_nav_menus' => true,
		'supports' => array(
			'title',
			'editor',
			'thumbnail'
		),
		'can_export' => true
	));
}



// Add scripts/styles
function master_scripts() {
	wp_enqueue_script( 'master', get_template_directory_uri().'/dist/js/master.js', array(),'1.0.0',tr );
}
add_action('wp_enqueue_scripts', 'master_scripts');
// Add admin scripts/styles
function load_admin_scripts() {
	wp_enqueue_script('admin-bundle', get_template_directory_uri().'/dist/js/admin.bundle.js',array(),'1.0.0',true);
	wp_enqueue_style('admin-styles', get_template_directory_uri().'/dist/css/admin.css');
}
add_action('admin_enqueue_scripts', 'load_admin_scripts');
function login_customization() {
  echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/dist/css/admin.css" />';
}
add_action('login_head', 'login_customization');
function my_login_logo_url() {
  return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
  return 'Wisetracker';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function vendor_scripts() {
   wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/dist/js/vendor/modernizr.min.js', array(), '1.0.0', true );
   wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/dist/js/vendor/lightbox.min.js', array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'vendor_scripts' );


add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
	<div id="workers-profile-edit">
		<h3><?php _e("Extra profile information", 'wisetracker'); ?></h3>
		<h4><?php _e("Availability", 'wisetracker'); ?></h4>
		<date-picker
      :controls="false"
      preselected-days="<?php echo esc_attr( implode( ',', get_user_meta( $user->ID, 'available_day' ) ) ); ?>"
      search-button="<?php _e( 'Search', 'wisetracker' ); ?>"
      clear-button="<?php _e( 'Clear', 'wisetracker' ); ?>"
			weekdays-translation="<?php _e( 'S,M,T,W,T,F,S', 'wisetracker' ); ?>"
			month-translation="<?php _e( 'January,February,March,April,May,June,July,August,September,October,November,December', 'wisetracker' ); ?>"
      ></date-picker>
		<br>
		<span class="description"><?php _e("Please enter days when you are available.", 'wisetracker'); ?></span>
		{{ initDays }}
	</div>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}

	// if ( ! delete_user_meta($user_id, 'available_day') ) {
	// 	echo "Ooops! Error while deleting this information!";
	// }
	delete_user_meta($user_id, 'available_day');
	// $days = explode(',',$_POST['availability']);
	$days = explode(',',$_POST['selectedDays']);
	foreach ($days as $day) {
		add_user_meta( $user_id, 'available_day', trim($day));
	}
	return true;
}

// add_action( 'rest_api_init', 'adding_user_meta_rest' );

// function adding_user_meta_rest() {
// 	register_rest_field( 'user', 'available_day', array(
// 		'get_callback'      => 'user_meta_callback',
// 		'update_callback'   => null,
// 		'schema'            => null,
// 	)
// );
// }
// function user_meta_callback( $user, $field_name, $request) {
// 	return get_user_meta( $user[ 'id' ], $field_name );
// }

/**
* Add REST API support to an already registered post type.
*/
add_action( 'init', 'my_custom_post_type_rest_support', 25 );
function my_custom_post_type_rest_support() {
	global $wp_post_types;

	//be sure to set this to the name of your post type!
	$post_type_name = 'tools';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
		$wp_post_types[$post_type_name]->show_in_rest = true;
		// Optionally customize the rest_base or controller class
		$wp_post_types[$post_type_name]->rest_base = $post_type_name;
		$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
}


// add_filter( 'rest_user_query', 'prefix_remove_has_published_posts_from_wp_api_user_query', 10, 2 );
// /**
// * Removes `has_published_posts` from the query args so even users who have not
// * published content are returned by the request.
// *
// * @see https://developer.wordpress.org/reference/classes/wp_user_query/
// *
// * @param array           $prepared_args Array of arguments for WP_User_Query.
// * @param WP_REST_Request $request       The current request.
// *
// * @return array
// */
// function prefix_remove_has_published_posts_from_wp_api_user_query( $prepared_args, $request ) {
// 	unset( $prepared_args['has_published_posts'] );
//
// 	return $prepared_args;
// }



function wp_json_workers_v2__init() {

	add_action('rest_api_init', function () {
		register_rest_route('workers/v2', '/users', array (
			'methods'             => 'GET',
			'callback'            => 'wp_json_workers_v2__list',
			'permission_callback' => function (WP_REST_Request $request) {
				return true;
			}
		));
	});

	function wp_json_workers_v2__list($request) {
		$parameters = $request->get_query_params();
		if (isset($parameters['available_days'])) {
			$available_days = explode(',',$parameters['available_days']);
			$countDays = count($available_days);
			$perPage = isset($parameters['per_page']) ? $parameters['per_page'] : 8 ;
			$pageOffset = isset($parameters['page']) ? --$parameters['page']*$perPage : 0 ;

			global $wpdb;
			// $sqlBase = "SELECT u.id, u.user_email AS email, COUNT(user_id) as dates_count FROM {$wpdb->prefix}usermeta AS m JOIN {$wpdb->prefix}users AS u ON u.ID = m.user_id WHERE meta_key = 'available_day' AND meta_value IN ('" . implode("', '", $available_days) . "') GROUP by user_id HAVING dates_count = {$countDays}";
			$sqlBase = "SELECT u.id, u.user_email AS email, pic.meta_value AS pic, COUNT(u.id) AS dates_count FROM {$wpdb->prefix}users AS u INNER JOIN {$wpdb->prefix}usermeta AS m ON u.ID = m.user_id AND m.meta_key = 'available_day' LEFT JOIN `{$wpdb->prefix}usermeta` AS pic ON u.ID = pic.user_id AND pic.meta_key = 'profile_picture' WHERE m.meta_value IN('" . implode("', '", $available_days) . "') GROUP BY u.ID HAVING dates_count = {$countDays}";
			$sql = "{$sqlBase} LIMIT {$pageOffset}, {$perPage}";
			$workersAll = $wpdb->get_results($sqlBase);

      // The following is without pagination & fetches all
      $workers = $wpdb->get_results($sqlBase);

      // Uncomment the following when pagination is added
			// $workers = $wpdb->get_results($sql);

			foreach ($workers as $worker) {
				if ($worker->pic) {
	        $worker->pic = wp_get_attachment_image_src($worker->pic, 'worker-profile')[0];
				}
				$worker->url = get_author_posts_url($worker->id);
			}
		}
		$data = array(
			'success' => true,
			'request' => $parameters,
			'count' => count($workers),
			'countAll' => count($workersAll),
			'users' => $workers
		);
		return new WP_REST_Response($data, 200);
	}
	// flush_rewrite_rules(true); // FIXME: <------- DONT LEAVE ME HERE
}

add_action('init', 'wp_json_workers_v2__init');

	// global $wpdb;
	// $sql = "SELECT u.id, u.user_email AS email, COUNT(user_id) as dates_count FROM {$wpdb->prefix}usermeta AS m JOIN {$wpdb->prefix}users AS u ON u.ID = m.user_id WHERE meta_key = 'available_day' AND meta_value IN ('2018-02-21','2018-02-20') GROUP by user_id HAVING dates_count = 2";
	// $results = $wpdb->get_results( $sql );

	// Removes admin color scheme options
  // Removes the leftover 'Visual Editor', 'Keyboard Shortcuts' and 'Toolbar' options.
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
add_action( 'admin_head', function () {
	ob_start( function( $subject ) {
		$subject = preg_replace( '#<h[0-9]>'.__("Personal Options", 'wisetracker').'</h[0-9]>.+?/table>#s', '', $subject, 1 );
		$subject = preg_replace( '#<h[0-9]>'.__("About Yourself", 'wisetracker').'</h[0-9]>.+?/table>#s', '', $subject, 1 );
		$subject = preg_replace( '#<h[0-9]>'.__("Translation options", 'wisetracker').'</h[0-9]>.+?/table>#s', '', $subject, 1 );
		return $subject;
	});
});

add_action( 'admin_footer', function(){ ob_end_flush(); });



// Building Menu

// wp_nav_menu( array( 'menu' => 'global-nav', 'walker' => new WPDocs_Walker_Nav_Menu() ) );

/**
 * Custom walker class.
 */
class WPDocs_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * Starts the list before the elements are added.
     *
     * Adds classes to the unordered list sub-menus.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // Depth-dependent classes.
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );

        // Build HTML for output.
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }

    /**
     * Start the element output.
     *
     * Adds main/sub-classes to the list items and links.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Depth-dependent classes.
        $depth_classes = array(
            ( $depth == 0 ? 'nav-item' : 'nav-item nav-item--2lvl' ),
            ( $depth >=2 ? 'nav-item nav-item--3lvl' : '' ),
            ( $depth % 2 ? 'nav-item--odd' : 'nav-item--even' ),
            'nav-item--lvl--' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        // Build HTML.
        $output .= $indent . '<li id="nav-item--'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // Link attributes.
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="nav-link ' . ( $depth > 0 ? 'nav-link--lvl'.$depth : '' ) . '"';

        // Build HTML output and pass through the proper filter.
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}
