<?php /* Template Name: Home page */ get_header(); ?>

<workers-search inline-template>
  <main :class="['main--global', { 'main--hasUsers': this.submitted }]">
    <div class="homepage-head">
      <div class="homepage-headInner">
        <div class="homepage-shout">
          <h1>WiseTracker</h1>
          <h2><?php _e( 'Please select days, when you will be needing workers', 'wisetracker' ); ?></h2>
        </div>

        <div class="userSearch userSearch--homepage">
          <header class="text--center">
            <h4 class="mb--1 hidden">...</h4>
            <p class="usersearch-title"><?php _e( 'Search', 'wisetracker' ); ?></p>
          </header>
          <date-picker
            @clicked="searchForWorkers"
            :controls="true"
            search-button="<?php _e( 'Search', 'wisetracker' ); ?>"
            clear-button="<?php _e( 'Clear', 'wisetracker' ); ?>"
            weekdays-translation="<?php _e( 'S,M,T,W,T,F,S', 'wisetracker' ); ?>"
            month-translation="<?php _e( 'January,February,March,April,May,June,July,August,September,October,November,December', 'wisetracker' ); ?>"
          ></date-picker>
        </div>

      </div>
    </div>
    <div class="container userCards" v-if="workers.length > 0">
      <h3 class="userCards-resultsTitle"><?php _e( 'Search results', 'wisetracker' ); ?></h3>
      <div class="userCards-list mb--3 mb--sm--5">
        <div class="mb--3 mb--sm--5" v-for="worker in workers">
          <a :href="worker.url" class="userCard">
            <img :src="worker.pic" :alt="worker.email" class="userCard-img" v-if="worker.pic" />
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/placeholder-250x250.jpg" :alt="worker.email" class="userCard-img" v-if="!worker.pic" />
            <span class="userCard-email">
              {{ worker.email }}
            </span>
          </a>
        </div>
        <div class="paginator">
          <?php _e( 'Total', 'wisetracker' ); ?>: <strong>{{ totalWorkers }} | {{ page }}/{{ pages }}</strong>
        </div>
      </div>
    </div>
    <div class="container userCards" v-if="noResults">
      <h3 class="userCards-resultsTitle"><?php _e( 'Results', 'wisetracker' ); ?></h3>
      <p class="text--leader mb--0"><?php _e( 'Sorry, nothing to display.', 'wisetracker' ); ?></p>
    </div>
    <div class="loader-mask" v-if="loader"><div class="loader--spinner"></div></div>
  </main>
</workers-search>

<?php get_footer(); ?>
