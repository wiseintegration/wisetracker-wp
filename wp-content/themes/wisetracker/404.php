<?php get_header(); ?>

<div class="page-head"></div>
<main class="main--global main--sidebars">
  <?php get_template_part('sidebar-left'); ?>
  <section class="content content--main text--center">
		<h1><?php _e( 'Page not found', 'wisetracker' ); ?></h1>
		<p class="text--leader">
			<a href="<?php echo home_url(); ?>" class="button button--main"><?php _e( 'Go back', 'wisetracker' ); ?></a>
		</p>

  </section>
	<?php get_template_part('sidebar-right'); ?>
</main>

<?php get_footer(); ?>
