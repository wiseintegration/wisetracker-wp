<aside class="sidebar sidebar--left">
  <h3 class=""><?php _e( 'Tools/materials', 'wisetracker' ); ?></h3>
  <div class="categories">
    <?php
    $args = array(
      'numberposts' => -1,
      'post_type'   => 'tools',
    );
    $tools = get_posts( $args );
    if($tools):
      foreach ($tools as $tool):
        ?>
        <a class="categories-item" href="<?= $tool->guid ?>"><?= $tool->post_title ?></a>
        <?php
      endforeach;
    else:
      ?>
      <p class="color--faded"><?php _e( 'No categories', 'wisetracker' ); ?></p>
    <?php endif; ?>
  </div>
</aside>
