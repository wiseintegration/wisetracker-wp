<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
  <div class="page-head"></div>
  <main class="main--global">

    <section class="content content--main">

      	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
      			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
      				<?php the_post_thumbnail(array(120,120)); ?>
      			</a>
      		<?php endif; ?>

      		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

      	</article>

    </section>

  </main>


<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<div>
		<h2><?php _e( 'Sorry, nothing to display.', 'wisetracker' ); ?></h2>
	</div>
	<!-- /article -->

<?php endif; ?>
<?php get_footer(); ?>
