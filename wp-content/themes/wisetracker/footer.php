

	<footer class="footer footer--global">
		<div class="footer-inner">
		  WiseTracker &copy; <?php _e( 'copyright', 'wisetracker' ); ?> <?= date('Y') ?> &middot; <?php _e( 'mady by', 'wisetracker' ); ?> <a href="http://www.wiseintegration.com" target="_blank">WiseIntegration</a>
		</div>
	</footer>
</div>
<?php wp_footer(); ?>
<!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/dist/js/master.js"></script> -->
	</body>
</html>
