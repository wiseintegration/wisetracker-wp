<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<link href="https://fonts.googleapis.com/css?family=Mukta+Mahee:300,400,700&amp;subset=latin-ext" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/master.css">
	</head>
	<?php
	if(get_field('page_class')) {
		$pageClass = 'page page--'.get_field('page_class');
	}
	else {
		$pageClass = 'page';
	}
	?>
	<body <?php body_class($pageClass); ?>>

		<!-- wrapper -->
		<div class="pageWrapper" id="app">

			<header class="header header--global">

				<nav class="nav nav--global">
					<a class="brand" href="<?php echo home_url(); ?>">
						<span class="brand-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-1.png" alt="" height="24px" class="">
						</span>
						<span class="brand-name ml--1"><strong>Wise</strong>Tracker</span>
					</a>

          <span class="nav-toggle" data-nav-toggle id="nav-toggle">
            <span class="fas fa-bars fa-fw"></span>
          </span>

					<div class="nav-collapse">
            <?php wp_nav_menu( array( 'theme_location' => 'global-nav', 'walker' => new WPDocs_Walker_Nav_Menu(), 'items_wrap' => '<ul class="nav-items">%3$s</ul>', 'container' => '' ) ); ?>
            <?php wp_nav_menu( array( 'theme_location' => is_user_logged_in() ? 'logged-in-menu' : 'logged-out-menu', 'walker' => new WPDocs_Walker_Nav_Menu(), 'items_wrap' => '<ul class="nav-items">%3$s</ul>', 'container' => '' ) ); ?>
						<ul class="nav-items">
							<li class="nav-item nav-item--hasSubnav">
								<a class="nav-link">
									<i class="fa fa-fw fa-globe"></i>
								</a>
								<div class="nav-subnav nav-subnav--languages">
									<?php echo qtranxf_generateLanguageSelectCode(‘both’); ?>
									<!-- <a class="nav-link" hreflang="" href="">lang</a> -->
									<!-- <a class="nav-link" hreflang="" href="">lang</a> -->
								</div>
							</li>
						</ul>
					</div>
				</nav>



			</header>
