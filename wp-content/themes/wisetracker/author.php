<?php get_header(); ?>

<div class="page-head"></div>
<main class="main--global main--sidebars">
  <?php get_template_part('sidebar-left'); ?>

  <section class="content content--main">

    <?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>
    <?php // print_r($curauth); ?>
    <?php // print_r($curauth->classification); ?>
    <div class="mb--3 mb--sm--5 text--center">
      <?php if($curauth->profile_picture): ?>
        <div class="user-profileImage" style="background-image:url(<?php echo wp_get_attachment_image_src($curauth->profile_picture, 'worker-profile')[0]; ?>)"></div>
      <?php else: ?>
        <div class="user-profileImage" style="background-image:url(<?php echo get_template_directory_uri(); ?>/dist/images/placeholder-250x250.jpg)"></div>
      <?php endif; ?>
    </div>
    <h1 class="text--center m--0"><?php echo $curauth->display_name; ?></h1>
    <hr>
    <div class="user-info">
      <div class="user-infoRow grid mb--2">
        <span class="col col--sm--6 text--sm--right">
          El. paštas:
        </span>
        <strong class="col col--sm--6"><?php echo $curauth->user_email; ?></strong>
      </div>
      <?php if($curauth->phone_number): ?>
        <div class="user-infoRow grid mb--2">
          <span class="col col--sm--6 text--sm--right">
            <?php _e( 'Phone number', 'wisetracker' ); ?>:
          </span>
          <strong class="col col--sm--6"><?php echo $curauth->phone_number; ?></strong>
        </div>
      <?php endif; ?>
      <?php if($curauth->description): ?>
        <div class="user-infoRow grid mb--2">
          <span class="col col--sm--6 text--sm--right">
            <?php _e( 'Description', 'wisetracker' ); ?>:
          </span>
          <strong class="col col--sm--6"><?php echo $curauth->description; ?></strong>
        </div>
      <?php endif; ?>
      <?php if($curauth->classification): ?>
        <div class="user-infoRow grid">
          <span class="col col--sm--6 text--sm--right">
            <?php _e( 'Categories', 'wisetracker' ); ?>:
          </span>
          <strong class="col col--sm--6">
            <?php echo implode(', ', $curauth->classification); ?>
          </strong>
        </div>
      <?php endif; ?>
    </div>
    <?php if( have_rows('gallery') ): ?>
      <h3 class="text--center">
        <?php _e( 'Gallery', 'wisetracker' ); ?>:
      </h3>
      <div class="user-images text--center">
        <?php while ( have_rows('gallery') ) : the_row(); ?>
          <a href="<?php echo wp_get_attachment_image_src(get_sub_field('image'), 'full')[0]; ?>" data-lightbox="gallery" <?php get_sub_field('description') ? 'data-title="'.get_sub_field('description').'"' : '' ?> >
            <img src="<?php echo wp_get_attachment_image_src(get_sub_field('image'), 'medium')[0]; ?>" <?php get_sub_field('description') ? 'alt="'.get_sub_field('description').'"' : '' ?>/>
          </a>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </section>
  <?php get_template_part('sidebar-right'); ?>
</main>


<?php get_footer(); ?>
