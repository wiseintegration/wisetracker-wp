const gulp = require('gulp');
const gutil = require('gulp-util');
const requireDir  = require('require-dir');

requireDir('./gulp/tasks', {recursive: false});
