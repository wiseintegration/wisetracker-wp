const resolve = require('path').resolve; // eslint-disable-line

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'wisetracker-wp',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  css: [
    './assets/sass/master.sass'
  ],

  modules: [
    ['nuxt-sass-resources-loader', resolve(__dirname, './assets/sass/global.sass')]
  ],

  plugins: ['./plugins/filters'],
  /*
  /*
  ** Customize the progress bar color
  */
  loading: { color: 'cyan' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
