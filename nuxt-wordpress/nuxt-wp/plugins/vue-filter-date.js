import Vue from 'vue'
import parse from 'date-fns/parse'
import format from 'date-fns/format'

Vue.filter('date-filter', (input) => input.every(elem => ['2018-12-12'].indexOf(elem) > -1))
Vue.filter('output-date', (input) => format(parse(input), 'MMMM D, YYYY h:mm a'))
